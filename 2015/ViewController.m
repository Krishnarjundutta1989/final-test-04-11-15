//
//  ViewController.m
//  2015
//
//  Created by click labs 115 on 11/4/15.
//  Copyright (c) 2015 cli. All rights reserved.
//

#import "ViewController.h"

@interface ViewController (){
    CLLocationManager *manager;
    NSString *formatedaddress;
    CLLocationCoordinate2D position;
    NSString *City;
    NSString *state;
    NSString *country;
    NSString *Name;
    NSString *State;
    GMSMarker *marker;
    NSString *latitude;
    NSString *longitude;
    NSString *address;
    NSString *massage;
    CLLocation *currentLocation;
    NSMutableArray *arrayForDetails;
    NSMutableArray *destarrayForDetails;
    NSString *destmassage;
    NSString *destName;
    NSString *destCity;
    NSString *destLat;
    NSString *destlon;
    NSString *destState;
    NSString *destaddress;
}
@property (strong, nonatomic) IBOutlet GMSMapView *googleMap;
@property (strong, nonatomic) IBOutlet UISearchBar *searchCity;

@end

@implementation ViewController
@synthesize googleMap;
@synthesize searchCity;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    arrayForDetails = [NSMutableArray new];
    manager = [[CLLocationManager alloc]init];
    manager.delegate = self;
    manager.desiredAccuracy = kCLLocationAccuracyBest;
    [manager startUpdatingLocation];
    [manager requestWhenInUseAuthorization];
    googleMap.delegate = self;
    
    // Do any additional setup after loading the view, typically from a nib.
}
- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"" message:@"Unable To Get Your Current Location" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
    
}
- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    
    NSLog(@"didUpdateToLocation: %@", newLocation);
    currentLocation = newLocation;

    
    if (currentLocation != nil) {
        
        position = currentLocation.coordinate;
        GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:currentLocation.coordinate.latitude
                                                                longitude:currentLocation.coordinate.longitude
                                                                     zoom:10.0];
        [googleMap animateToCameraPosition:camera];
        marker = [GMSMarker markerWithPosition:position];
        marker.title = [NSString stringWithFormat:@"%@",formatedaddress];
        
        marker.map = googleMap;
        [self getaddress];
        
        
    }
    //[manager stopUpdatingLocation];
}
-(void) getaddress  {
    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
    
    CLLocation *newLocation = [[CLLocation alloc]initWithLatitude:currentLocation.coordinate.latitude
                                                        longitude:currentLocation.coordinate.longitude];



    
    [geocoder reverseGeocodeLocation:newLocation
                   completionHandler:^(NSArray *placemarks, NSError *error) {
                       
                       if (error) {
                           NSLog(@"Geocode failed with error: %@", error);
                           return;
                       }
                       
                       if (placemarks && placemarks.count > 0)
                       {
                           CLPlacemark *placemark = placemarks[0];
                           
                           NSDictionary *addressDictionary =
                           placemark.addressDictionary;
                           
                           NSLog(@"%@ ", addressDictionary);
                           
                           if (addressDictionary[@"Country"]!= NULL){
                               country = addressDictionary[@"Country"];
                               NSLog(@"%@",country);
                               


                               
                               if ([country isEqualToString:@"India"]) {
                                   massage = @"You Are In India";
                                   [arrayForDetails addObject:[NSString stringWithFormat:@"%@",massage]];
                               }
                               else{
                                   massage = @"You Are OutSide India";
                                   [arrayForDetails addObject:[NSString stringWithFormat:@"%@",massage]];

                               }
                               
                               [arrayForDetails addObject:[NSString stringWithFormat:@"%@",country]];

                           }
                          if (addressDictionary[@"Name"]!= NULL) {
                               Name = addressDictionary[@"Name"];
                               NSLog(@"%@",Name);
                              [arrayForDetails addObject:[NSString stringWithFormat:@"%@",Name]];

                           }
                          else{
                              Name = @"N.A";
                              NSLog(@"%@",Name);
                              [arrayForDetails addObject:[NSString stringWithFormat:@"%@",Name]];
                              

                          }
                          if (addressDictionary[@"State"]!= NULL) {
                              State = addressDictionary[@"State"];
                              NSLog(@"%@",State);
                              [arrayForDetails addObject:[NSString stringWithFormat:@"%@",State]];

                          }
                          else{
                              State = @"N.A";
                              NSLog(@"%@",State);
                              [arrayForDetails addObject:[NSString stringWithFormat:@"%@",State]];
                              
                              
                          }
                          if (addressDictionary[@"City"]!= NULL) {
                              City = addressDictionary[@"City"];
                              NSLog(@"%@",City);
                              [arrayForDetails addObject:[NSString stringWithFormat:@"%@",City]];
                              
                          }
                          else{
                              City = @"N.A";
                              NSLog(@"%@",City);
                              [arrayForDetails addObject:[NSString stringWithFormat:@"%@",City]];
                              
                              
                          }

                          if (addressDictionary[@"FormattedAddressLines"] != NULL){
                              NSArray *FormattedAddressLines = addressDictionary[@"FormattedAddressLines"];
                            
                                 formatedaddress = [FormattedAddressLines objectAtIndex:0];
                              [arrayForDetails addObject:[NSString stringWithFormat:@"%@",formatedaddress]];

                              NSLog(@"%@",arrayForDetails);
                          }

                       }
                       
                       
                   }];
    latitude = [NSString stringWithFormat:@"%f",currentLocation.coordinate.latitude];
    [arrayForDetails addObject:[NSString stringWithFormat:@"%@",latitude]];
    longitude = [NSString stringWithFormat:@"%f",currentLocation.coordinate.longitude];
    [arrayForDetails addObject:[NSString stringWithFormat:@"%@",longitude]];

    
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    GetDetailsViewController *VC = segue.destinationViewController;
    VC.destarrayForDetails = arrayForDetails;
    
}

    - (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
