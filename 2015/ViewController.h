//
//  ViewController.h
//  2015
//
//  Created by click labs 115 on 11/4/15.
//  Copyright (c) 2015 cli. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GoogleMaps/GoogleMaps.h>
#import "GetDetailsViewController.h"


@interface ViewController : UIViewController<GMSMapViewDelegate,CLLocationManagerDelegate,UISearchBarDelegate>


@end

